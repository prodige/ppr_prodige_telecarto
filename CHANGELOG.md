# Changelog

All notable changes to [ppr_prodige_telecarto](https://gitlab.adullact.net/prodige/ppr_prodige_telecarto) project will be documented in this file.

## [4.4.4](https://gitlab.adullact.net/prodige/ppr_prodige_telecarto/compare/4.4.3...4.4.4) - 2023-11-21

## [4.4.3](https://gitlab.adullact.net/prodige/ppr_prodige_telecarto/compare/4.4.2...4.4.3) - 2023-07-28

## [4.4.2](https://gitlab.adullact.net/prodige/ppr_prodige_telecarto/compare/4.4.1...4.4.2) - 2023-07-25

## [4.4.1](https://gitlab.adullact.net/prodige/ppr_prodige_telecarto/compare/4.4.0...4.4.1) - 2023-07-24

## [4.4.0](https://gitlab.adullact.net/prodige/ppr_prodige_telecarto/compare/4.4.0_rc14...4.4.0) - 2023-03-30

## [4.4.0_rc14](https://gitlab.adullact.net/prodige/ppr_prodige_telecarto/compare/4.4.0_rc13...4.4.0_rc14) - 2022-12-08
