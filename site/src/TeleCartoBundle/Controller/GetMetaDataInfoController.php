<?php

namespace ProdigeTeleCarto\TeleCartoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\DAOProxy\DAO;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @abstract service qui permet de récupérer les informations d'une métadonnée
 * @param metadata_id   identifiant de la métadonnée (REQUEST)
 * @return chaîne json contenant les informations de la métadonnée encodées en base64
 * @Route("/telecarto")
 * @author Alkante
 */
class GetMetaDataInfoController extends BaseController
{

    /**
     * @Route("/getMetaDataInfo", name="telecarto_getMetaDataInfo", options={"expose"=true})
     */
    public function getMetaDataInfoAction(Request $request)
    {

        $result['success'] = false;

        // récupère l'identifiant de la métadonnée
        if ($request->get("metadata_id", "") != "") {
            $metadata_id = $request->get("metadata_id");
        } else {
            echo json_encode($result);
            exit();
        }

        //require_once("../Administration/DAO/DAO/DAO.php");

        $strSql = /*"set search_path to public;" .*/
            " SELECT m.id, m.uuid, m.data FROM metadata as m" .
            //" LEFT JOIN relations as r ON m.id=r.id" .
            //" LEFT JOIN metadata as c ON r.relatedid=c.id" .
            " WHERE m.id=?";

        //$dao = new DAO();
        $conn = $this->getCatalogueConnection('public');
        $dao = new DAO($conn);

        if ($dao) {
            $rs = $dao->BuildResultSet($strSql, array($metadata_id));
            if ($rs->GetNbRows() > 0) {
                $rs->First();
                if (!$rs->EOF()) {
                    $result['success'] = true;
                    $result['metadata_id'] = base64_encode($rs->Read(0));
                    $result['metadata_uuid'] = base64_encode($rs->Read(1));

                    //On ne remplace pas les caracteres html contenus dans le xml (probleme de decodage du xml sinon notamment avec les chevrons < et >)
                    $result['metadata_data'] = $rs->ReadHtml(2);
                    $result['metadata_data'] = base64_encode($result['metadata_data']);

                    //$result['metadata_data']  = base64_decode($rs->ReadHtml(2));
                    /*$result['catalogue_id']   = base64_encode($rs->Read(3));
                    $result['catalogue_uuid'] = base64_encode($rs->Read(4));
                    $result['catalogue_data'] = base64_encode($rs->Read(5));*/

                    //TODO hismail - Benoist
                    /*require_once ('../include/geonetworkInterface.php');
                    global $PRO_GEONETWORK_DIRECTORY;
                    $gntw = new Geonetwork();

                    $strUrl = $PRO_GEONETWORK_DIRECTORY."/srv/fre/xml.relation?type=fcat&fast=false&id=".$rs->Read(0);
                    $xmlDoc = new DOMDocument('1.0', 'UTF-8');

                    $strXml = $gntw->get($strUrl);
                    if ( $xmlDoc->loadXML($strXml) === TRUE ) {
                        $relations = $xmlDoc->getElementsByTagName('relation');
                        if($relations->length>0){
                            for ( $i=0; $i<$relations->length; $i++ ) {
                                if ( $relations->item($i)->hasAttribute('type') && $relations->item($i)->getAttribute('type') =="fcats" ) {
                                    $fcatMetadata = $relations->item($i)->getElementsByTagName("id")->item(0)->nodeValue;
                                    $strSqlCat = "set search_path to public;" .
                                        " SELECT c.id, c.uuid, c.data FROM metadata as c" .
                                        //" LEFT JOIN relations as r ON m.id=r.id" .
                                    //" LEFT JOIN metadata as c ON r.relatedid=c.id" .
                                    " WHERE c.id=".$fcatMetadata;
                                    $rsCat = $dao->BuildResultSet($strSqlCat);
                                    if ( $rsCat->GetNbRows() > 0 ) {
                                        $rsCat->First();
                                        if ( !$rsCat->EOF() ) {
                                            $result['catalogue_id']   = base64_encode($rs->Read(0));
                                            $result['catalogue_uuid'] = base64_encode($rs->Read(1));
                                            $result['catalogue_data'] = base64_encode($rs->Read(2));
                                        }
                                    }
                                }
                            }
                        }
                    }*/
                }
            }
        }

        echo json_encode($result);
        exit();

    }

}