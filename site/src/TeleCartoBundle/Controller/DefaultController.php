<?php

namespace ProdigeTeleCarto\TeleCartoBundle\Controller;

use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\Stream;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use Prodige\ProdigeBundle\Controller\GetMetaDataPDFCompletController;
use ProdigeTeleCarto\TeleCartoBundle\Common\Queue;

use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @author alkante <support@alkante.com>
 */
class DefaultController extends BaseController
{
    /**
     *
     * @Route("/", name="telecarto_index", options={"expose"=true})
     */
    public function telecartoIndexAction(Request $request)
    {
        set_time_limit(0);
        Queue::initController($this);
        $service_idx = $request->get("service_idx", "");
        $tabParams = array();
        $mode = $request->get("mode", "");
        switch ($mode) {
            case "prodige" :
                $tabParams = $this->prodige_get_params($request);
                break;
            default :
                $tabParams = $this->carmen_get_params($request);
                break;
        }
        if (isset($tabParams['direct']) && $tabParams['direct'] == "1") {
            $bDirect = true;
        } else {
            $bDirect = false;
        }

        if (isset($tabParams['fileRedirect']) && $tabParams['fileRedirect'] == "1") {
            $fileRedirect = true;
        } else {
            $fileRedirect = false;
        }
        $returnJSON = $this->exec_telechargement($bDirect, $tabParams, $fileRedirect, $service_idx);
        if ($returnJSON instanceof Response) return $returnJSON;
        $callback = $request->get("callback", "");
        if ($callback !== "") {
            return new Response($callback . '(' . json_encode($returnJSON) . ')');
        } else {
            return new JsonResponse($returnJSON);
        }
    }


    /**
     * @Route("/list_queue", name="telecarto_service", options={"expose"=true})
     */
    public function telecartoServiceAction(Request $request)
    {

        Queue::initController($this);
        $service = $request->get("service", "");
        $callback = $request->get("callback", "");
        if ($service == "listQueue") {
            $queue = new Queue(DIR_QUEUE);
            $json_object = array();
            $json_object["listQueue"] = $queue->list_queue();
            $json_object["colone"] = ['index', 'demandeur', 'format', 'projection', 'tables', 'id metadata', 'type', 'champ 8', 'champ 9', 'extraction spatiale'
                , 'type d\'extraction', 'territoire d\'extraction', 'zone tampon (m)', 'emprise d\'extraction', '15', '16', '17', '18', 'date / heure', '19', '20', 'ref'];
            $json_object["index"] = [18, 1, 6, 4, 2, 3, 5, 9, 10, 11, 13, 12, 21];
            return new Response($callback . '(' . json_encode($json_object) . ")");

        } elseif ($service == "delfile") {
            $file_name = $request->get("filename");
            $queue = new Queue(DIR_QUEUE);
            $filename = DIR_QUEUE . "/" . $file_name;
            $queue->remove_from_queue($filename);
            return new Response($callback . "({success:true})");
        } elseif ($service == "delallfile") {
            $queue = new Queue(DIR_QUEUE);
            $queue->remove_all_from_queue();
            return new Response($callback . "({success:true})");
        }
    }

    /***********************************************************************
     * @abstract fonction qui récupère les paramètres pour le mode prodige *
     * @return tableau contenant les paramètres                            *
     ***********************************************************************/
    protected function prodige_get_params(Request $request)
    {

        $tabRes = array();

        $tabRes['email'] = urldecode(base64_decode($request->get('email', '')));
        $tabRes['format'] = urldecode(base64_decode($request->get('format', '')));
        $tabRes['data'] = urldecode(base64_decode($request->get('data', '')));
        $tabRes['metadata_id'] = urldecode(base64_decode($request->get('metadata_id', '')));
        $tabRes['projection'] = urldecode(base64_decode($request->get('projection', '')));
        $tabRes['direct'] = urldecode(base64_decode($request->get('direct', '')));

        $tabRes['fileRedirect'] = $request->get('fileRedirect', '');
        $tabRes['data_type'] = $request->get('data_type', '');
        $tabRes['bTerritoire'] = $request->get('bTerritoire', '');
        $tabRes['territoire_type'] = $request->get('territoire_type', '');
        $tabRes['territoire_data'] = $request->get('territoire_data', '');
        $tabRes['extractionattributaire_couche'] = $request->get('extractionattributaire_couche', '');
        $tabRes['extract_area'] = $request->get('extract_area', '');
        $tabRes['restricted_area_field'] = $request->get('restricted_area_field', '');
        $tabRes['restricted_area_buffer'] = $request->get('restricted_area_buffer', '');
        $tabRes['territoire_area'] = $request->get('territoire_area', '');
        $tabRes['buffer'] = $request->get('buffer', '');
        $tabRes['id_ens_serie'] = $request->get('id_ens_serie', '');

        $tabRes['metadata_file'] = "";
        $user = \Prodige\ProdigeBundle\Controller\User::GetUser();
        $tabRes['user_id'] = $user->GetLogin();


        return $tabRes;
    }

    /***********************************************************************
     * @abstract fonction qui récupère les paramètres pour le mode carmen  *
     * @return tableau contenant les paramètres                            *
     ***********************************************************************/
    protected function carmen_get_params(Request $request)
    {
        $tabRes = array();
        $token = $request->get("token", "");
        $tabParams = explode("&", $token);
        foreach ($tabParams as $strParam) {
            $tabParam = array();
            if ($strParam == "")
                continue;
            $tabParam = explode("=", $strParam);
            if (count($tabParam) == 2) {
                $tabRes[$tabParam[0]] = $tabParam[1];
            }
        }
        return $tabRes;
    }

    /*********************************************************************************************************
     * @abstract fonction qui ajoute la demande à la file de téléchargement                                  *
     * @param bDirect     booléen indiquant si le traitement et le téléchargement se fait directement ou pas *
     * @param tabParams   tableau des paramètres
     * @param fileRedirect redirection vers le fichier généré                                                     *
     * @return objet Json                                                                                    *
     *********************************************************************************************************/
    protected function exec_telechargement($bDirect, $tabParams, $fileRedirect = 0, $service_idx)
    {
        //global $service_idx;
        //global $service;

        $metadata_path = PRO_DATA_PATH . "/cartes/METADATA/Publication/";
        $strParams = "";
        foreach ($tabParams as $field => $value) {
            $strParams .= $field . "=" . $value . ";";
        }

        $result = array('success' => false,
            'msg' => 'Échec dans la procédure de téléchargement.',
            'params' => $strParams);

        $fname = "";
        if (isset($tabParams['email']) && isset($tabParams['format']) && isset($tabParams['data']) && isset($tabParams['projection'])) {
            $queue = new Queue(DIR_QUEUE);
            $typeInput = Queue::$VECTOR_FILE;
            $data = $tabParams['data'];
            if (strpos($tabParams['data'], "POSTGIS_DATA") === false) {
                if ($tabParams['data_type'] == 'raster') {
                    $typeInput = Queue::$RASTER_FILE;
                } else if ($tabParams['data_type'] == 'raster_hr') {
                    $typeInput = Queue::$RASTER_HR_FILE;
                } else if ($tabParams['data_type'] == 'majic') {
                    $typeInput = Queue::$MAJIC_FILE;
                } else if ($tabParams['data_type'] == 'url') {
                    $typeInput = Queue::$URL;
                } else if ($tabParams['data_type'] == 'model') {
                    $typeInput = Queue::$MODEL;
                }
            } else {
                if ($tabParams['data_type'] == 'table') {
                    $typeInput = Queue::$TABLE_POSTGIS;
                } else if ($tabParams['data_type'] == 'vector') {
                    $typeInput = Queue::$VECTOR_POSTGIS;
                }
                $data = explode(":", $tabParams['data']);
                $data = $data[1];
            }

            // Postgis connection is usefull and necessary for geom computation stuff
            // even when data are not stred in postgis
            //$pgConnection = loadPgConnect(URL_PATH_DATA.$service["path"]."IHM/Postgis/");
            $conn = $this->getConnection('prodige');
            $pgConnection = "\"PG:host=" . $conn->getHost() .
                " user=" . $conn->getUsername() .
                " dbname=" . $conn->getDatabase() .
                " password=" . $conn->getPassword() . "\"";

            $fname = $queue->add_to_queue($service_idx,
                $tabParams['email'],
                $tabParams['format'],
                ($tabParams['projection'] ? "EPSG:" . $tabParams['projection'] : ""),
                $data,
                $tabParams['metadata_id'],
                $typeInput,
                $pgConnection,
                $metadata_path . $tabParams['metadata_file'],
                $tabParams['bTerritoire'],
                $tabParams['territoire_type'],
                $tabParams['territoire_data'],
                $tabParams['buffer'],
                $tabParams['extract_area'],
                (isset($tabParams['restricted_area_field']) ? $tabParams['restricted_area_field'] : null),
                (isset($tabParams['restricted_area_buffer']) ? $tabParams['restricted_area_buffer'] : null),
                (isset($tabParams['territoire_area']) ? $tabParams['territoire_area'] : null),
                $tabParams['extractionattributaire_couche'],
                (isset($tabParams['id_ens_serie']) ? $tabParams['id_ens_serie'] : null),
                (isset($tabParams['user_id']) ? $tabParams['user_id'] : null));

            if ($fname) {
                $result['success'] = true;
                if ($bDirect) {
                    $urlZip = $queue->process($fname);
                    $result['msg'] = 'Le traitement est terminé. Vous pouvez télécharger les données à l\'adresse suivante : ' . $urlZip;
                    $result['data'] = $urlZip;
                    if ($fileRedirect) {
                        return new RedirectResponse($urlZip);
                    }
                } else {
                    $result['msg'] = 'Votre demande est prise en compte.';
                }
                $tabMetadataInfo = explode("%", $tabParams['metadata_id']);
                //revent doublons for same metadata (multiple tables)
                $tabMetadataInfo = array_unique($tabMetadataInfo);
                foreach ($tabMetadataInfo as $key => $metadataId) {
                    $metadata = $queue->getMetadataInfo($metadataId);
                    $user = \Prodige\ProdigeBundle\Controller\User::GetUser();
                    \Prodige\ProdigeBundle\Services\Logs::AddLogs('Telechargements', array(
                        $user->GetNom(), $user->GetPrenom(), $user->GetLogin(), $metadata['metadata_uuid'], $metadataId, $tabParams['format'], $tabParams['projection']
                    ));
                }
            }
        }
        return $result;
    }

    /**
     * @Route("/DOWNLOAD/{file}", name="telecarto_get_file", options={"expose"=true}, requirements={"file"=".+"})
     */
    public function getFileAction(Request $request, $file)
    {
        $link = DIR_DOWNLOAD . "/" . $file;
        if (!file_exists($link)) {
            return new Response("");
        }
        $stream = new Stream($link);
        $response = new BinaryFileResponse($stream);
        $response->trustXSendfileTypeHeader();
        $response->prepare($request);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $file, iconv('UTF-8', 'ASCII//TRANSLIT', $file));
        return $response;
    }

    public function setContainer(\Psr\Container\ContainerInterface $container): ?\Psr\Container\ContainerInterface
    {
        $this->container = $container;
        return $this->container;
    }

    public function getContainer()
    {
        return $this->container;
    }
}
