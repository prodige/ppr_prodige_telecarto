<?php

namespace ProdigeTeleCarto\TeleCartoBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

use ProdigeTeleCarto\TeleCartoBundle\Common\Queue;
use Prodige\ProdigeBundle\Controller\BaseController;


class QueueCommand extends Command
{

    private $container;

    /**
     * Configures the command definition
     */
    protected function configure()
    {
        $this->setName('prodigetelecarto:treatQueue')
            ->setDescription('lanch donwload queue');
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $configReader = $this->container->get('prodige.configreader');

        // simulates a call to onKernelController
        $c = new \ProdigeTeleCarto\TeleCartoBundle\EventListener\ControllerListener();
        $c->setContainer($this->getContainer());

        $c->onKernelController(new \Symfony\Component\HttpKernel\Event\ControllerEvent(
            $this->getContainer()->get('kernel'),
            function () {
            },
            new \Symfony\Component\HttpFoundation\Request(),
            \Symfony\Component\HttpKernel\HttpKernelInterface::MASTER_REQUEST
        ));

        $controller = new \ProdigeTeleCarto\TeleCartoBundle\Controller\DefaultController();
        $controller->setContainer($this->getContainer());

        Queue::initController($controller);

        $queue = new Queue(DIR_QUEUE);

        $queue->process_queue();

        return Command::SUCCESS;
    }

    public function setContainer($container): void
    {
        $this->container = $container;
    }

    public function getContainer()
    {
        return $this->container;
    }

}