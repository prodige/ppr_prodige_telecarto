<?php

namespace ProdigeTeleCarto\TeleCartoBundle\EventListener;

use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpKernel\Event\ControllerEvent;

class ControllerListener implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    protected function getDoctrine()
    {
        return $this->container->get('doctrine');
    }

    protected function getTemplating()
    {
        return $this->container->get('templating');
    }

    /**
     * Get Doctrine connection
     * @param string $name
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($name, $schema = "public")
    {
        $conn = $this->getDoctrine()->getConnection($name);
        $conn->exec('set search_path to ' . $schema);
        return $conn;
    }

//    public function onKernelController(FilterControllerEvent $event) {
    public function onKernelController(ControllerEvent $event)
    {
        if (!$event->isMasterRequest())
            return;

        /*
         * Download server parameters
         *
         */

        define("PRO_SCHEMA_PARAMETRAGE_NAME", "parametrage");

        /**
         * Download server name  (overloaded)
         */
        define("TELE_CARTO_NAME", "Prodige telechargement");

        /**
         *
         * ogr2ogr command path
         */
        define("OGR_PATH", "/usr/bin/ogr2ogr");

        /**
         *
         * DOWNLOAD data path
         */
        define("DIR_DOWNLOAD", $this->container->get('prodige.configreader')->getParameter('PRO_ROOT_DOWNLOAD_TELECARTO_ATOM'));
        try {
            @mkdir(DIR_DOWNLOAD, 0777, true);
        } catch (\Exception $e) {
        }
        /**
         *
         * DOWNLOAD data url
         */
        define("URL_SERVER_DOWNLOAD", CARMEN_URL_SERVER_DOWNLOAD . "/DOWNLOAD");

        /**
         *
         * QUEUE data path
         */
        define("DIR_QUEUE", rtrim($this->container->getParameter("PRODIGE_PATH_DATA"), "/") . "/QUEUE");
        try {
            @mkdir(DIR_QUEUE, 0777, true);
        } catch (\Exception $e) {
        }

        /**
         *
         * data path
         */
        define("URL_PATH_DATA", PRO_DATA_PATH);

        //TODO hismail
        /**
         * Sender mail (download)(overloaded)
         */
        define("ADMINISTRATOR_MAIL", "admin@prodige.fr");

        /**
         * Add licence file
         */
        define("ADD_LICENCE_FILE", true);

        /**
         * Licence file constant name in BD
         */
        define("LICENCE_CONSTANT_NAME", "PRO_METADATA_LICENCE_PATH");

        /**
         * Active adding metadata file
         */
        define("ADD_METADATA_FILE", false);

        //TODO hismail
        /**
         * chemin de base du fichier de projection
         */
        define("PATH_PROJ_FILE", "/applications/lib/share/proj/epsg.carmen");

        //TODO hismail
        define("GDAL_TRANSLATE_PATH", "/usr/bin/gdal_translate");
        define("GDAL_WARP_PATH", "/usr/bin/gdalwarp");

        //include_once("parametrage_sgbd.php");
        /*get prodige parameters from database*/

        //update global variables from table settings
        $conn = $this->getConnection('prodige', 'parametrage');

        $query = "SELECT prodige_settings_constant, prodige_settings_value from prodige_settings;";
        $result = $conn->fetchAllAssociative($query);
        foreach ($result as $row) {
            //eval("$".$row['prodige_settings_constant']." = '".$row['prodige_settings_value']."';");
            if (!defined($row['prodige_settings_constant']))
                define($row['prodige_settings_constant'], $row['prodige_settings_value']);
        }
        $conn = $this->getConnection('catalogue', 'catalogue');

        $query = "SELECT prodige_settings_constant, prodige_settings_value from prodige_settings;";
        $result = $conn->fetchAllAssociative($query);
        foreach ($result as $row) {
            //eval("$".$row['prodige_settings_constant']." = '".$row['prodige_settings_value']."';");
            if (!defined($row['prodige_settings_constant']))
                define($row['prodige_settings_constant'], $row['prodige_settings_value']);
        }


    }
}