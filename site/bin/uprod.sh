#!/bin/bash
set -e

# Source env file
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`

# gen assets
OWNER="www-data"
function run_as {
  su -pc "$1" -s /bin/bash $OWNER
}

#Clear cache
run_as "php $SCRIPTDIR/console cache:clear --env dev"
run_as "php $SCRIPTDIR/console cache:clear --env prod"
