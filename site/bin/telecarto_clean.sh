#!/bin/bash
DATE=`date '+%y%m%d'`
SCRIPTDIR=`dirname $(readlink -f $0)`
SCRIPTNAME=`basename $(readlink -f $0)`
HOMEPRODIGE="/var/www/html/site"
# Log folder
LOGDIR="${HOMEPRODIGE}/var/log/cron"; [ -d ${HOMEPRODIGE}/var/logs/cron ] || mkdir -p "${HOMEPRODIGE}/var/logs/cron"

LOG="${LOGDIR}/${SCRIPTNAME%.*}_${DATE}.log"
LOCKDIR="${LOGDIR}/${SCRIPTNAME%.*}.lock"

echolog()
{
  echo "`date '+%b %d %H:%M:%S'`  ${1}"
}

lock()
{
   mkdir "${LOCKDIR}" 2> /dev/null && trap 'rm -rf ${LOCKDIR}; exit' 0 2
}
lock || { echolog "Erreur ${SCRIPTNAME} est déjà en cours d'exécution, fin du script"; exit 1;}

mainjob()
{
  [ -d ${HOMEPRODIGE}/public/DOWNLOAD ] && find ${HOMEPRODIGE}/public/DOWNLOAD/ -mindepth 1 -mtime +7 -exec rm -rf {} \;
  [ -d ${HOMEPRODIGE}/public/DOWNLOAD ] && find ${HOMEPRODIGE}/public/DOWNLOAD/ -mindepth 1 -maxdepth 1 -type d -mtime +1 -exec rm -rf {} \;
  [ -d ${HOMEPRODIGE}/public/DOWNLOAD ] && find ${HOMEPRODIGE}/public/DOWNLOAD/ -maxdepth 1 -type f -mtime +1 -name "*.zip.*" -delete
}

exec 6>&1 7>&2 && exec >> ${LOG} 2>&1
echolog "Starting ${SCRIPTDIR}/${SCRIPTNAME}"
mainjob
find ${LOGDIR}/ -name "${SCRIPTNAME%.*}_*.log" -type f -mtime +10 -exec rm -f {} \;
echolog "End of ${SCRIPTDIR}/${SCRIPTNAME}"
exec 1>&6 6>&- 2>&7 7>&-
