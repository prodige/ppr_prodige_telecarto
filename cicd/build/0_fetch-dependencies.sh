#!/bin/sh
set -e

# Source env file
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`
. $SCRIPTDIR/env.sh

# Fix default env value if needed
if [ -z "${NEXUS_USR}" ]; then
  echo -n "Enter nexus.alkante.com login: "
  read NEXUS_USR
fi
if [ -z "${NEXUS_PSW}" ]; then
  echo -n "password: "
  stty -echo
  IFS= read -r NEXUS_PSW
  stty echo
  printf '\n'
fi
[ -z "${REPO_COMPOSER_DOMAIN}" ] && REPO_COMPOSER_DOMAIN='nexus.alkante.com'

echo -n "LOCAL_USER_ID=$(id -u)\nLOCAL_GROUP_ID=$(id -g)" > $SCRIPTDIR/docker-build/docker-compose.env;

# Create dockers
docker-compose -f $SCRIPTDIR/docker-build/docker-compose.yml build
docker-compose -f $SCRIPTDIR/docker-build/docker-compose.yml up -d
sleep 2

# Exec
docker exec --user www-data -w /var/www/html/site ppr_prodige_telecarto_build_web /bin/bash -c " \
  set -e; \
  sleep 1; \
  echo '{
    \"http-basic\": {
      \""$REPO_COMPOSER_DOMAIN"\": {
			  \"username\": \""$NEXUS_USR"\",
			  \"password\": \""$NEXUS_PSW"\"
      }
    }
  }' > auth.json; \
  composer install --no-interaction --no-progress; \
  rm auth.json"

# Stop dockers
docker-compose -f $SCRIPTDIR/docker-build/docker-compose.yml down
