# Build

The build step is a pipeline decribe in ```Jenkinsfile```.

The step a runned in order with all bash script:
- [number]_[script_name].sh


If an error occur, use the ```99_clean.sh``` to clean all and revert rigths.

## Test the docker prod
```bash
# Login on Alkante docker registry
docker login docker.alkante.com

# Create dockers
docker-compose -f ./cicd/build/docker-prod/docker-compose.yml up
```
url : http://localhost:4200

# Stop dockers
```bash
docker-compose -f ./cicd/build/docker-build/docker-compose.yml down
```