#!/bin/bash
# Script pour mettre à jour le nom du projet
# en se basant sur le nom de dossier se trouvant
# avant le dossier cicd
# Exemple pour /www/spr_my_new_project/cicd/
# oldName = ppr_prodige_telecarto
# newName = spr_my_new_project
#
# Usage : ./update-name.sh

cicdPath=$(dirname $(readlink -f $0))
oldName=$(cat $cicdPath/project_name.txt)
newName=$(basename $(dirname $cicdPath))
if [[ "$oldName" != "" && "$newName" != "" ]]; then
	echo "$oldName -> $newName"
	grep -rl "$oldName" . | xargs sed -i "s/$oldName/$newName/g"
	echo -n "$newName" > $cicdPath/project_name.txt
else
	echo "Erreur de nom"
	echo "oldName: '$oldName'"
	echo "newName: '$newName'"
fi
