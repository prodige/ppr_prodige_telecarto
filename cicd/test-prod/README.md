
## Run

Run with
```bash
docker-compose up
```
url : http://localhost:4200

Stop with
```bash
docker-compose down
```