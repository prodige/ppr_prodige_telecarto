#!/bin/bash
SCRIPTDIR=`dirname $(readlink -f $0)`

ROOT_GRP="root"
APACHE="www-data"
OWNER="www-data"
CURRENT_USER_ID=`id -u`
PARAM_USER="${1}"

if [ $(getent passwd "jenkins") ]; then
  OWNER="jenkins"
else
  if [ $(getent passwd "ansible-dev") ]; then
    OWNER="ansible-dev"
  else
    if [ $(getent passwd "ftpuser") ]; then
      OWNER="ftpuser"
    else
      if [ -n PARAM_USER ] && [ $(getent passwd "${PARAM_USER}") ]; then
         # cas developpeur
         OWNER="${PARAM_USER}"
         ROOT_GRP=`id -g "${PARAM_USER}"`
      else
         OWNER="${CURRENT_USER_ID}"
      fi
    fi
  fi
fi

rights()
{
    if [ -d "${1}" ]
    then
        echo "Chowning ${1}"
        find "${1}" -print0 | xargs -0 chown ${2}.${3}
        if [ "${4}" == "ro" ]; then
            echo "Chmoding ${1} directories ${4}"
            find "${1}" -type d -print0 | xargs -0 chmod 750
            echo "Chmoding ${1} files ${4}"
            find "${1}" -type f -print0 | xargs -0 chmod 640
        fi
        if [ "${4}" == "rx" ]; then
            echo "Chmoding ${1} directories ${4}"
            find "${1}" -type d -print0 | xargs -0 chmod 750
            echo "Chmoding ${1} files ${4}"
            find "${1}" -type f -print0 | xargs -0 chmod 750
        fi
        if [ "${4}" == "rw" ]; then
            echo "Chmoding ${1} directories ${4}"
            find "${1}" -type d -print0 | xargs -0 chmod 770
            echo "Chmoding ${1} files ${4}"
            find "${1}" -type f -print0 | xargs -0 chmod 660
        fi
        if [ "${4}" == "rwx" ]; then
            echo "Chmoding ${1} directories ${4}"
            find "${1}" -type d -print0 | xargs -0 chmod 770
            echo "Chmoding ${1} files ${4}"
            find "${1}" -type f -print0 | xargs -0 chmod 770
        fi
    fi
}

chown ${OWNER}.${APACHE} "${SCRIPTDIR}/site"
rights "${SCRIPTDIR}/site" "${OWNER}" "${APACHE}" "ro"
mkdir -p "${SCRIPTDIR}/site/var"
rights "${SCRIPTDIR}/site/var" "${OWNER}" "${APACHE}" "rw"
rights "${SCRIPTDIR}/site/public/DOWNLOAD/" "${OWNER}" "${APACHE}" "rw"
chmod 755 "${SCRIPTDIR}"

find "${SCRIPTDIR}/site" -maxdepth 1 -name "*.sh" -type f -exec chown "${OWNER}".root {} \;
find "${SCRIPTDIR}/site" -maxdepth 1 -name "*.sh" -type f -exec chmod 750 {} \;

find "${SCRIPTDIR}/site/bin" -name "*.sh" -type f -exec chown "${OWNER}".root {} \;
find "${SCRIPTDIR}/site/bin" -name "*.sh" -type f -exec chmod 700 {} \;

# Scripts named telecarto* are called by ${APACHE}
find "${SCRIPTDIR}/site/bin" -name "telecarto*.sh" -type f -exec chgrp "${APACHE}" {} \;
find "${SCRIPTDIR}/site/bin" -name "telecarto*.sh" -type f -exec chmod 750 {} \;
