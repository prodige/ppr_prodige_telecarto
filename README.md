# Module PRODIGE téléchargement

Module de téléchargement de PRODIGE, intégrant le traitement à la volée et par file d'attente des demandes de téléchargement

### configuration

Modifier le fichier site/app/config/global_parameters.yml

### Installation

[documentation d'insatllation en développement](cicd/dev/README.md).

## Accès à l'application

- pas d'accès direct, accès depuis le catalogue
